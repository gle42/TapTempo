AUTHOR
Ghislain LECOMTE
User "gle" on linuxfr.org
@gle42 on GitLab

CONTACT
https://linuxfr.org/board or GitLab

LICENSE
This code is under GPL 3.0
See gpl-3.0.txt file for license details.

COMPILING
Install maven and run "mvn clean install" in the directory where pom.xml is.

RUNNING
java -cp target/taptempo-1-full.jar com.i2bp.taptempo.TapTempo
(plus any additional parameters)

DISCLAIMER
No warranties. This was a quick and dirty exercise and the code is rather crude.
